#!/bin/bash

# Get list of all monitors.
monitors=($(xrandr --query | grep " connected" | cut -d " " -f1))

# Assign each monitor its desktops.
# Desktops are spread evenly among all monitors with monitors getting one each,
# then going back to the first monitor.
# e.g. for 2 monitors, monitor 1 gets 1 3 5 7 9, monitor 2 gets 2 4 6 8 10
# for 3 monitors: 1 gets 1 4 7 0, 2 gets 2 5 8, 3 gets 3 6 9
for i in $(seq 1 ${#monitors[*]})
do
        commands[i]=""
        for j in {1..10}
        do
                if [ "$(($j % ($i + ${#monitors[*]} - 1)))" -eq "0" ]
                then
                        commands[i]+="$j "
                fi
        done
        bspc monitor ${monitors[$(($i-1))]} -d ${commands[*]}
done
